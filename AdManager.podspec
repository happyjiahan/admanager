Pod::Spec.new do |s|
  s.name         = 'AdManager'
  s.version      = '0.1.7'
  s.summary      = 'Manager for Ad Control'
  s.author = {
    'http://blog.codingcoder.com' => 'http://blog.codingcoder.com'
  }
  s.source = {
    :git => 'https://happyjiahan@bitbucket.org/happyjiahan/admanager.git',
    :tag => 'happyjiahanV0.1.7'
  }
  s.homepage    = 'http://blog.codingcoder.com'
  s.license     = 'LICENSE'
  s.source_files = '*.{h,m}'
  s.platform = :ios
  s.ios.deployment_target = '7.0'
  s.requires_arc = true
end
