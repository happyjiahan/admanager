//
//  JHAdManager+AdMob.h
//  avoid
//
//  Created by LIJIAHAN on 14-6-24.
//  Copyright (c) 2014年 codingcoder. All rights reserved.
//

#import "JHAdManager.h"

@interface JHAdManager (AdMob)

#pragma mark - AdMob
- (void)admob_showInterstitialFromRootViewController:(UIViewController *)rootViewController;

- (void)admob_showBannerFromRootViewController:(UIViewController *)rootViewController
                                          view:(UIView *)view adSize:(GADAdSize)size origin:(CGPoint)origin;

@end
