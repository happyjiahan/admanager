//
//  JHAdManager+AdMob.m
//  avoid
//
//  Created by LIJIAHAN on 14-6-24.
//  Copyright (c) 2014年 codingcoder. All rights reserved.
//

#import "JHAdManager+AdMob.h"

@implementation JHAdManager (AdMob)

#pragma mark -
#pragma mark - AdMob

#pragma mark - GADBannerView

- (BOOL)admob_needShowBannerView
{
    if (_hasReceivedBannerAd && [[NSDate date] timeIntervalSinceDate:_lastShowBannerTime] > _intervalForShowBanner) {
        return YES;
    } else {
        return NO;
    }
}


- (void)admob_showBannerFromRootViewController:(UIViewController *)rootViewController
                                          view:(UIView *)view adSize:(GADAdSize)size origin:(CGPoint)origin
{
    if (!_admob_bannerView) {
        _admob_bannerView = [[GADBannerView alloc] initWithAdSize:size origin:origin];
        _admob_bannerView.delegate = self;
        
        // 指定广告单元ID。
        _admob_bannerView.adUnitID = Admob_BannerView_ADUnitID;
        
        // 告知运行时文件，在将用户转至广告的展示位置之后恢复哪个UIViewController
        // 并将其添加至视图层级结构。
        _admob_bannerView.rootViewController = rootViewController;
        
        _admob_bannerView.hidden = YES;
        [view addSubview:_admob_bannerView];
        
//        // 启动一般性请求并在其中加载广告。
//        [_admob_bannerView loadRequest:[GADRequest request]];
    } else {
        
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            // 启动一般性请求并在其中加载广告。
            [_admob_bannerView loadRequest:[GADRequest request]];
        });
        
        if ([self admob_needShowBannerView]) {
            _admob_bannerView.hidden = NO;
            
            // 记下展示时间
            _lastShowBannerTime = [NSDate date];
        } else {
            _admob_bannerView.hidden = YES;
        }
    }
}

#pragma mark - GADBannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    _hasReceivedBannerAd = YES;
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    _admob_bannerView.hidden = YES;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(30 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_admob_bannerView loadRequest:[GADRequest request]];
    });
}

- (void)adViewDidDismissScreen:(GADBannerView *)adView
{
    [_admob_bannerView loadRequest:[GADRequest request]];
}




#pragma mark - GADInterstitial

- (BOOL)admob_needShowInterstitial
{
    if ([[NSDate date] timeIntervalSinceDate:_lastShowInterstitalTime] > _intervalForShowInterstitial) {
        return YES;
    } else {
        return NO;
    }
}

- (void)admob_showInterstitialFromRootViewController:(UIViewController *)rootViewController
{
    if ([self admob_needShowInterstitial]) {
        if ([self admob_hasReadyInterstitialAd]) {
            GADInterstitial *interstitial = [_admob_readyInterstitialAdSet anyObject];
            [interstitial presentFromRootViewController:rootViewController];
            [_admob_readyInterstitialAdSet removeObject:interstitial];
            
            _lastShowInterstitalTime = [NSDate date];
        } else {
            // fetch another ad
            if ([self admob_needCacheAndFetchAdRequest]) {
                [self admob_cacheAndFetchAdRequest];
            }
        }
        
    } else {
        
        if ([self admob_needCacheAndFetchAdRequest]) {
            [self admob_cacheAndFetchAdRequest];
        }
    }
}

- (BOOL)admob_hasReadyInterstitialAd
{
    return [_admob_readyInterstitialAdSet count];
}

- (BOOL)admob_hasCachedInterstitialAd
{
    return [_admob_cachedInterstitialAdSet count];
}

- (BOOL)admob_needCacheAndFetchAdRequest
{
    if (![self admob_hasCachedInterstitialAd] && ![self admob_hasReadyInterstitialAd]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)admob_cacheAndFetchAdRequest
{
    GADInterstitial *ad = [[GADInterstitial alloc] init];
    ad.delegate = self;
    ad.adUnitID = Admob_Interstitial_ADUnitID;
    GADRequest *request = [GADRequest request];
    [ad loadRequest:request];
    if (ad) {
        [_admob_cachedInterstitialAdSet addObject:ad];
    }
}


#pragma mark - GADInterstitialDelegate
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad
{
    if (ad) {
        [_admob_readyInterstitialAdSet addObject:ad];
        [_admob_cachedInterstitialAdSet removeObject:ad];
    }
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    [_admob_cachedInterstitialAdSet removeObject:ad];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(30 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (![self admob_hasCachedInterstitialAd]) {
            [self admob_cacheAndFetchAdRequest];
        }
    });
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad
{
    if ([self admob_needCacheAndFetchAdRequest]) {
        [self admob_cacheAndFetchAdRequest];
    }
}



@end
