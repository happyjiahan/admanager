//
//  JHAdManager+Chartboost.h
//  avoid
//
//  Created by LIJIAHAN on 14-6-24.
//  Copyright (c) 2014年 codingcoder. All rights reserved.
//

#import "JHAdManager.h"

@interface JHAdManager (Chartboost)

#pragma mark - Chartboost
- (void)chartboost_regist;
- (void)chartboost_showInterstitial:(CBLocation)location;
- (void)chartboost_cacheInterstitial:(CBLocation)location;

@end
