//
//  JHAdManager+Chartboost.m
//  avoid
//
//  Created by LIJIAHAN on 14-6-24.
//  Copyright (c) 2014年 codingcoder. All rights reserved.
//

#import "JHAdManager+Chartboost.h"

@implementation JHAdManager (Chartboost)

#pragma mark -
#pragma mark - Chartboost
- (void)chartboost_regist
{
    // Begin a user session. Must not be ependent on user actions or any prior network requests.
    // Must be called every time your app becomes active.
    [Chartboost startWithAppId:Chartboost_AppID appSignature:Chartboost_AppSignature delegate:self];
}

- (void)chartboost_showInterstitial:(CBLocation)location
{
    if ([self chartboost_needShowInterstitial]) {
        if ([[Chartboost sharedChartboost] hasCachedInterstitial:location]) {
            [[Chartboost sharedChartboost] showInterstitial:location];
            _lastShowInterstitalTime = [NSDate date];
            
        } else {
            [self chartboost_cacheInterstitial:location];
        }
    } else {
        if (![[Chartboost sharedChartboost] hasCachedInterstitial:location]) {
            [self chartboost_cacheInterstitial:location];
        }
    }
}

- (void)chartboost_cacheInterstitial:(CBLocation)location
{
    if (!_chartboost_isCachingInterstitial) {
        [[Chartboost sharedChartboost] cacheInterstitial:location];
        _chartboost_isCachingInterstitial = YES;
    }
}

- (BOOL)chartboost_needShowInterstitial
{
    if ([[NSDate date] timeIntervalSinceDate:_lastShowInterstitalTime] > _intervalForShowInterstitial) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - ChartboostDelegate
- (void)didCacheInterstitial:(CBLocation)location
{
    _chartboost_isCachingInterstitial = NO;
}

- (void)didFailToLoadInterstitial:(CBLocation)location  withError:(CBLoadError)error
{
    _chartboost_isCachingInterstitial = NO;
}

- (void)didDismissInterstitial:(CBLocation)location
{
    [self chartboost_cacheInterstitial:location];
}

@end
