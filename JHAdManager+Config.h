//
//  JHAdManager+Config.h
//  avoid
//
//  Created by LIJIAHAN on 14-6-24.
//  Copyright (c) 2014年 codingcoder. All rights reserved.
//

#import "JHAdManager.h"
#import <AVOSCloud.h>

#pragma mark -
#pragma mark - JHAdConfig
@interface JHAdConfig : AVObject <AVSubclassing>
@property (nonatomic, copy) NSString *adName;
@property (nonatomic, assign) float adPriority;
@property (nonatomic, assign) float intervalForShowInterstitial;
@property (nonatomic, assign) float intervalForShowBanner;
@end


#pragma mark -
#pragma mark - JHAdManager
@interface JHAdManager (Config)
- (void)setDefaultAdConfigs;
- (void)fetchAdConfig;
- (BOOL)needShowAdMobForNextAd;
@end
