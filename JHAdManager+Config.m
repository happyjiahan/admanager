//
//  JHAdManager+Config.m
//  avoid
//
//  Created by LIJIAHAN on 14-6-24.
//  Copyright (c) 2014年 codingcoder. All rights reserved.
//

#import "JHAdManager+Config.h"

#pragma mark -
#pragma mark - JHAdConfig
@implementation JHAdConfig

+ (NSString *)parseClassName {
    return @"AdConfig";
}

@end



#pragma mark -
#pragma mark - JHAdManager
@implementation JHAdManager (Config)

- (void)setDefaultAdConfigs
{
    JHAdConfig *admobConfig = [[JHAdConfig alloc] init];
    admobConfig.adName = @"admob";
    admobConfig.adPriority = 0.7;
    admobConfig.intervalForShowInterstitial = 120;
    admobConfig.intervalForShowBanner = 120;
    
    JHAdConfig *chartboostConfig = [[JHAdConfig alloc] init];
    chartboostConfig.adName = @"chartboost";
    chartboostConfig.adPriority = 0.3;
    chartboostConfig.intervalForShowInterstitial = 120;
    chartboostConfig.intervalForShowBanner = 120;
    
    _adConfigs = @[admobConfig, chartboostConfig];
    _intervalForShowInterstitial = admobConfig.intervalForShowInterstitial;
    _intervalForShowBanner = admobConfig.intervalForShowBanner;
    
}

- (void)fetchAdConfig
{
    AVQuery *query = [JHAdConfig query];
    query.cachePolicy = kAVCachePolicyCacheThenNetwork;
    [query orderByAscending:@"adName"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            _adConfigs = objects;
            
            JHAdConfig *config = [_adConfigs firstObject];
            if (config) {
                _intervalForShowInterstitial = config.intervalForShowInterstitial;
                _intervalForShowBanner = config.intervalForShowBanner;
            }
        }
    }];
}

- (BOOL)needShowAdMobForNextAd
{
    int n = arc4random() % 10;
    
    JHAdConfig *firstAdPlatform = [_adConfigs firstObject];
    int firstBound = firstAdPlatform.adPriority * 10 + 0.5;
    if (n < firstBound) {
        return YES;
    } else {
        return NO;
    }
}


@end
