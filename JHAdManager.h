//
//  JHAdManager.h
//  avoid
//
//  Created by LIJIAHAN on 14-6-24.
//  Copyright (c) 2014年 codingcoder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Chartboost.h>
#import <GADBannerView.h>
#import <GADInterstitial.h>
#import <GADInAppPurchase.h>
#import <GADAdSize.h>

// admob
#define Admob_BannerView_ADUnitID_Avoid         @"ca-app-pub-9852877405163761/9269934731"
#define Admob_Interstitial_ADUnitID_Avoid       @"ca-app-pub-9852877405163761/1746667933"

#define Admob_BannerView_ADUnitID               Admob_BannerView_ADUnitID_Avoid
#define Admob_Interstitial_ADUnitID             Admob_Interstitial_ADUnitID_Avoid


// chartboost
#define Chartboost_AppID_Avoid                  @"53aa20f11873da3232576782"
#define Chartboost_AppSignature_Avoid           @"732ec1722324cbad42dab99575d5c7000de2c371"

#define Chartboost_AppID_Inline                 @"539544a689b0bb3aa47319f6"
#define Chartboost_AppSignature_Inline          @"16707edc0f39711be49fc1e2bce3fef8bd47490f"

#define Chartboost_AppID_Pongo                  @"539531661873da32bd8fb17f"
#define Chartboost_AppSignature_Pongo           @"94cfe266cc3539ff2695fead2785290c7d6b94b8"

//#define Chartboost_AppID                    Chartboost_AppID_Avoid
//#define Chartboost_AppSignature             Chartboost_AppSignature_Avoid

#define Chartboost_AppID                        Chartboost_AppID_Pongo
#define Chartboost_AppSignature                 Chartboost_AppSignature_Pongo


@interface JHAdManager : NSObject
<GADBannerViewDelegate, GADInterstitialDelegate,
ChartboostDelegate>
{
@private
    // config
    BOOL _hasReceivedBannerAd;
    NSDate *_lastShowBannerTime;
    NSDate *_lastShowInterstitalTime;
    NSTimeInterval _intervalForShowBanner; // 间隔多长时间再展示banner
    NSTimeInterval _intervalForShowInterstitial;
    
    NSString *_idfa;
    NSArray *_adConfigs;
    
    
    // admob
    NSMutableSet    *_admob_cachedInterstitialAdSet;
    NSMutableSet    *_admob_readyInterstitialAdSet;
    
    GADBannerView   *_admob_bannerView;
    
    // chartboost
    BOOL _chartboost_isCachingInterstitial;
    
}

+ (instancetype)sharedAdManager;
- (void)regist;

- (void)showInterstitialFromRootViewController:(UIViewController *)rootViewController;

- (void)showBannerFromRootViewController:(UIViewController *)rootViewController
                                          view:(UIView *)view adSize:(GADAdSize)size origin:(CGPoint)origin;

@end
