//
//  JHAdManager.m
//  avoid
//
//  Created by LIJIAHAN on 14-6-24.
//  Copyright (c) 2014年 codingcoder. All rights reserved.
//

#import "JHAdManager.h"
#import "JHAdManager+Config.h"
#import "JHAdManager+AdMob.h"
#import "JHAdManager+Chartboost.h"

@implementation JHAdManager

+ (instancetype)sharedAdManager
{
    static JHAdManager *adManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!adManager) {
            adManager = [[JHAdManager alloc] init];
        }
    });
    
    return adManager;
}

- (instancetype)init
{
    if (self = [super init]) {
        _admob_cachedInterstitialAdSet = [NSMutableSet set];
        _admob_readyInterstitialAdSet = [NSMutableSet set];
        
        // 默认的时间间隔
        _lastShowBannerTime = [NSDate date];
        _lastShowInterstitalTime = [NSDate date];
        _intervalForShowBanner = 3 * 60;
        _intervalForShowInterstitial = 2 * 60;
        
        _idfa = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
        
        [self setDefaultAdConfigs];
        [self fetchAdConfig];
        
    }
    return self;
}

- (void)regist
{
    [self chartboost_regist];
}

- (void)showInterstitialFromRootViewController:(UIViewController *)rootViewController
{
    if ([self needShowAdMobForNextAd]) {
        [self admob_showInterstitialFromRootViewController:rootViewController];
    } else {
        [self chartboost_showInterstitial:CBLocationStartup];
    }
}

- (void)showBannerFromRootViewController:(UIViewController *)rootViewController
                                          view:(UIView *)view adSize:(GADAdSize)size origin:(CGPoint)origin
{
    [self admob_showBannerFromRootViewController:rootViewController view:view adSize:size origin:origin];
}

@end
