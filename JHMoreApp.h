//
//  JHMoreApp.h
//  avoid
//
//  Created by LIJIAHAN on 14-6-26.
//  Copyright (c) 2014年 codingcoder. All rights reserved.
//

#import <AVOSCloud/AVOSCloud.h>

#define Key_Name            @"name"
#define Key_Desc            @"desc"
#define Key_CoverUrl        @"coverUrl"
#define Key_Appid           @"appid"
#define Key_Order           @"order"

@interface JHMoreApp : AVObject <AVSubclassing>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *appid;
@property (nonatomic, copy) NSString *coverUrl;
@property (nonatomic, assign) int order;

@end
