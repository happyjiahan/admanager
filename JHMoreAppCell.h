//
//  JHMoreAppCell.h
//  avoid
//
//  Created by LIJIAHAN on 14-6-26.
//  Copyright (c) 2014年 codingcoder. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JHMoreApp;
@interface JHMoreAppCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UIImageView *downloadImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

- (void)refreshCell:(JHMoreApp *)app;

@end