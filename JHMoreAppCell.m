//
//  JHMoreAppCell.m
//  avoid
//
//  Created by LIJIAHAN on 14-6-26.
//  Copyright (c) 2014年 codingcoder. All rights reserved.
//

#import "JHMoreAppCell.h"
#import <UIImageView+WebCache.h>
#import "JHMoreApp.h"

@implementation JHMoreAppCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundView = [[UIView alloc] init];
    self.backgroundView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.backgroundView.layer.borderWidth = 0.5;
}

- (void)refreshCell:(JHMoreApp *)app
{
    [self.coverImageView setImageWithURL:[NSURL URLWithString:app.coverUrl]];
    self.nameLabel.text = app.name;
}

@end
