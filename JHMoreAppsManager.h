//
//  JHMoreAppsManager.h
//  avoid
//
//  Created by LIJIAHAN on 14-6-26.
//  Copyright (c) 2014年 codingcoder. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JHMoreAppsManager : NSObject
@property (nonatomic, strong) NSArray *apps;

+ (instancetype)sharedMoreAppsManager;

@end
