//
//  JHMoreAppsManager.m
//  avoid
//
//  Created by LIJIAHAN on 14-6-26.
//  Copyright (c) 2014年 codingcoder. All rights reserved.
//

#import "JHMoreAppsManager.h"
#import "JHMoreApp.h"
#import <SDWebImageManager.h>

@implementation JHMoreAppsManager

+ (instancetype)sharedMoreAppsManager
{
    static JHMoreAppsManager *moreAppsManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!moreAppsManager) {
            moreAppsManager = [[JHMoreAppsManager alloc] init];
        }
    });
    
    return moreAppsManager;
}

- (instancetype)init
{
    if (self = [super init]) {
        [self setDefaultMoreApps];
        [self fetchMoreApps];
    }
    return self;
}

- (void)setDefaultMoreApps
{
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"moreApps" ofType:@"plist"];
    NSArray *appDics = [NSArray arrayWithContentsOfFile:plistPath];
    
    NSMutableArray *list = @[].mutableCopy;
    for (NSDictionary *dic in appDics) {
        JHMoreApp *app = [[JHMoreApp alloc] init];
        app.name = dic[Key_Name];
        app.desc = dic[Key_Desc];
        app.coverUrl = dic[Key_CoverUrl];
        app.appid = dic[Key_Appid];
        app.order = [dic[Key_Order] intValue];
        
        [list addObject:app];
    }
    
    if ([list count] > 1) {
        NSSortDescriptor *orderDescriptor = [NSSortDescriptor sortDescriptorWithKey:Key_Order ascending:YES];
        _apps = [list sortedArrayUsingDescriptors:@[orderDescriptor]];
    } else {
        _apps = list;
    }
}

- (void)fetchMoreApps
{
    AVQuery *query = [JHMoreApp query];
    query.cachePolicy = kAVCachePolicyCacheThenNetwork;
    [query orderByAscending:@"order"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            _apps = objects;
            
            for (JHMoreApp *app in _apps) {
                NSURL *url = [NSURL URLWithString:app.coverUrl];
                if (url) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [[SDWebImageManager sharedManager] downloadWithURL:url
                               options:0 progress:nil
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                             {
                                 // do nothing
                             }];
                    });
                }
            }
        }
    }];
}

@end
