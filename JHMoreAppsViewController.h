//
//  JHMoreAppsViewController.h
//  avoid
//
//  Created by LIJIAHAN on 14-6-26.
//  Copyright (c) 2014年 codingcoder. All rights reserved.
//

@import UIKit;
@import StoreKit;

@interface JHMoreAppsViewController : UICollectionViewController
<UICollectionViewDelegateFlowLayout, SKStoreProductViewControllerDelegate>
@property (nonatomic, strong) NSArray *apps;

@end
