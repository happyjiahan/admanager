//
//  JHMoreAppsViewController.m
//  avoid
//
//  Created by LIJIAHAN on 14-6-26.
//  Copyright (c) 2014年 codingcoder. All rights reserved.
//

#import "JHMoreAppsViewController.h"
#import "JHMoreAppCell.h"
#import <UIImage-Helpers.h>
#import "JHMoreApp.h"
#import "SKStoreProductViewController+StatusBarFix.h"
#import <MBProgressHUD.h>

@interface JHMoreAppsViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end

@implementation JHMoreAppsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeButtonClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_apps count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JHMoreAppCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MoreAppCell" forIndexPath:indexPath];
    [cell refreshCell:_apps[indexPath.row]];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"MoreAppsHeaderView" forIndexPath:indexPath];
    return header;
}


#pragma mark - UICollectionViewFlowLayout
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    JHMoreApp *app = _apps[indexPath.row];
    if ([app.appid length]) {
        [self openAppWithId:app.appid];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
}

- (void)openAppWithId:(NSString *)appid
{
    SKStoreProductViewController *storeVC = [[SKStoreProductViewController alloc] init];
    storeVC.delegate = self;
    [storeVC loadProductWithParameters:@{SKStoreProductParameterITunesItemIdentifier: appid}
                       completionBlock:^(BOOL result, NSError *error) {
                           
                           [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                           if (result) {
                               [self presentViewController:storeVC
                                                  animated:YES
                                                completion:nil];
                               
                           }
                           else{
                               NSLog(@"%@",error);
                           }
                           
                       }];
}
#pragma mark - SKStoreProductViewControllerDelegate
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES
                                       completion:nil];
}

@end
